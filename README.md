# RetoTecnicoChoucair

Reto técnico de automatización empresa Choucair

## Java version
17.0.2

## ChromeDriver 
100.0.4896.60

## Nombre proyecto local
co.com.choucair.retotecnico

## IntelliJ IDEA 2019.2.4 (Community Edition)
Build #IC-192.7142.36, built on October 29, 2019
Runtime version: 11.0.4+10-b304.77 amd64
VM: OpenJDK 64-Bit Server VM by JetBrains s.r.o

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/WilliamAguirre/retotecnicochoucair.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/WilliamAguirre/retotecnicochoucair/-/settings/integrations)

