package co.com.choucair.retotecnico.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class RegisterUserPage extends PageObject {
    public static final Target FIRST_NAME = Target.the("Campo primer nombre").located(By.id("firstName"));
    public static final Target LAST_NAME = Target.the("Campo apellido").located(By.id("lastName"));
    public static final Target EMAIL_ADRESS = Target.the("Campo email").located(By.id("email"));
    public static final Target MONTH = Target.the("Mes de nacimiento").located(By.xpath("//*[@name='birthMonth']//descendant::option[8]"));
    public static final Target DAY = Target.the("Dia de nacimiento").located(By.xpath("//*[@name='birthDay']//descendant::option[25]"));
    public static final Target YEAR = Target.the("Año de nacimiento").located(By.xpath("//*[@name='birthYear']//descendant::option[8]"));
    public static final Target LANGUAGES_INPUT = Target.the("Lenguajes").located(By.xpath("//*[@id='languages']/div[1]/input"));
    public static final Target NEXT_ONE = Target.the("Siguiente formulario").located(By.xpath("//a[@class='btn btn-blue']"));

    public static final Target INPUT_CITY = Target.the("Campo ciudad").located(By.id("city"));
    public static final Target INPUT_CP = Target.the("Codigo postal").located(By.id("zip"));
    public static final Target INPUT_COUNTRY = Target.the("Pais").located(By.xpath("//*[@id='regs_container']/div/div[2]/div/div[2]/div/form/div[1]/div[3]/div[1]/div[4]/div[2]/div/div/div[1]/span/span[2]"));
    public static final Target INPUT_COUNTRY_TXT = Target.the("Pais").located(By.xpath("//*[@id='regs_container']/div/div[2]/div/div[2]/div/form/div[1]/div[3]/div[1]/div[4]/div[2]/div/div/input[1]"));
    public static final Target NEXT_DEV = Target.the("formulario 3").located(By.xpath("//a[@class='btn btn-blue pull-right']"));
    public static final Target MOV_DEV = Target.the("Dispositivo movil").located(By.xpath("//*[@id='mobile-device']/div[1]/div[2]/div/div[1]/span"));
    public static final Target MOV_DEV_TXT = Target.the("Dispositivo movil").located(By.xpath("//*[@id='mobile-device']/div[1]/div[2]/div/input[1]"));
    public static final Target MOD_DEV = Target.the("Modelo de dispositivo").located(By.xpath("//*[@id='mobile-device']/div[2]/div[2]/div/div[1]/span"));
    public static final Target MOD_DEV_TXT = Target.the("Modelo de dispositivo").located(By.xpath("//*[@id='mobile-device']/div[2]/div[2]/div/input[1]"));
    public static final Target SO_DEV = Target.the("Sistema operativo").located(By.xpath("//*[@id='mobile-device']/div[3]/div[2]/div/div[1]/span"));
    public static final Target SO_DEV_TXT = Target.the("Sistema operativo").located(By.xpath("//*[@id='mobile-device']/div[3]/div[2]/div/input[1]"));
    public static final Target LAST_FORM = Target.the("Ultimo formulario").located(By.xpath("//*[@id='regs_container']/div/div[2]/div/div[2]/div/div[2]/div/a"));
    public static final Target PASS = Target.the("Contraseña").located(By.id("password"));
    public static final Target CONF_PASS = Target.the("Confirmar contraseña").located(By.id("confirmPassword"));
    public static final Target TERM = Target.the("Terminos y condiciones").located(By.xpath("//*[@id='regs_container']/div/div[2]/div/div[2]/div/form/div[5]/label/span[1]"));
    public static final Target PRIVACY = Target.the("Privacidad").located(By.xpath("//*[@id='regs_container']/div/div[2]/div/div[2]/div/form/div[6]/label/span[1]"));

    public static final Target COMPLET_BUTTON = Target.the("Completar").located(By.id("laddaBtn"));
}
