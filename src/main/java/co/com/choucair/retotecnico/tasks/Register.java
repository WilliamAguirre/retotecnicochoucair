package co.com.choucair.retotecnico.tasks;

import co.com.choucair.retotecnico.userinterface.RegisterUserPage;
import co.com.choucair.retotecnico.userinterface.UTestLoginPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.DoubleClick;
import net.serenitybdd.screenplay.actions.Enter;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Sleeper;

public class Register implements Task {
    private String strFirstName;
    private String strLastName;
    private String strEmail;
    private String strLanguage;
    private String strCity;
    private String strZip;
    private String strCountry;
    private String strDev;
    private String strMod;
    private String strSo;
    private String strPass;

    public Register(String strFirstName, String strLastName, String strEmail, String strLanguage, String strCity, String strZip, String strCountry, String strDev, String strMod, String strSo, String strPass) {
        this.strFirstName = strFirstName;
        this.strLastName = strLastName;
        this.strEmail = strEmail;
        this.strLanguage = strLanguage;
        this.strCity = strCity;
        this.strZip = strZip;
        this.strCountry = strCountry;
        this.strDev = strDev;
        this.strMod = strMod;
        this.strSo = strSo;
        this.strPass = strPass;
    }

    public static Register the(String strFirstName, String strLastName, String strEmail, String strLanguage, String strCity, String strZip, String strCountry, String strDev, String strMod, String strSo, String strPass) {
        return Tasks.instrumented(Register.class, strFirstName, strLastName, strEmail, strLanguage, strCity, strZip, strCountry, strDev, strMod, strSo, strPass);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Enter.theValue(strFirstName).into(RegisterUserPage.FIRST_NAME),
                Enter.theValue(strLastName).into(RegisterUserPage.LAST_NAME),
                Enter.theValue(strEmail).into(RegisterUserPage.EMAIL_ADRESS),
                Click.on(RegisterUserPage.MONTH),
                Click.on(RegisterUserPage.DAY),
                Click.on(RegisterUserPage.YEAR),
                Enter.theValue(strLanguage+ Keys.ENTER).into(RegisterUserPage.LANGUAGES_INPUT),
                Click.on(RegisterUserPage.NEXT_ONE),
                DoubleClick.on(RegisterUserPage.INPUT_CITY),
                Enter.theValue(Keys.CLEAR+strCity).into(RegisterUserPage.INPUT_CITY),
                Enter.theValue(strZip).into(RegisterUserPage.INPUT_CP),
                Click.on(RegisterUserPage.INPUT_COUNTRY),
                Enter.theValue(strCountry+Keys.ENTER).into(RegisterUserPage.INPUT_COUNTRY_TXT),
                Click.on(RegisterUserPage.NEXT_DEV),
                Click.on(RegisterUserPage.MOV_DEV),
                Enter.theValue(strDev+Keys.ENTER).into(RegisterUserPage.MOV_DEV_TXT),
                Click.on(RegisterUserPage.MOD_DEV),
                Enter.theValue(strMod+Keys.ENTER).into(RegisterUserPage.MOD_DEV_TXT),
                Click.on(RegisterUserPage.SO_DEV),
                Enter.theValue(strSo+Keys.ENTER).into(RegisterUserPage.SO_DEV_TXT),
                Click.on(RegisterUserPage.LAST_FORM),
                Enter.theValue(strPass).into(RegisterUserPage.PASS),
                Enter.theValue(strPass).into(RegisterUserPage.CONF_PASS),
                Click.on(RegisterUserPage.TERM),
                Click.on(RegisterUserPage.PRIVACY),
                Click.on(RegisterUserPage.COMPLET_BUTTON));
    }
}
