package co.com.choucair.retotecnico.Model;

public class UTestData {
    private String strFirstName;
    private String strLastName;
    private String strEmail;
    private String strLanguage;
    private String strCity;
    private String strZip;
    private String strCountry;
    private String strDev;
    private String strMod;
    private String strSo;
    private String strPass;

    public String getStrFirstName() {
        return strFirstName;
    }

    public void setStrFirstName(String strFirstName) {
        this.strFirstName = strFirstName;
    }

    public String getStrLastName() {
        return strLastName;
    }

    public void setStrLastName(String strLastName) {
        this.strLastName = strLastName;
    }

    public String getStrEmail() {
        return strEmail;
    }

    public void setStrEmail(String strEmail) {
        this.strEmail = strEmail;
    }

    public String getStrLanguage() {
        return strLanguage;
    }

    public void setStrLanguage(String strLanguage) {
        this.strLanguage = strLanguage;
    }

    public String getStrCity() {
        return strCity;
    }

    public void setStrCity(String strCity) {
        this.strCity = strCity;
    }

    public String getStrZip() {
        return strZip;
    }

    public void setStrZip(String strZip) {
        this.strZip = strZip;
    }

    public String getStrCountry() {
        return strCountry;
    }

    public void setStrCountry(String strCountry) {
        this.strCountry = strCountry;
    }

    public String getStrDev() {
        return strDev;
    }

    public void setStrDev(String strDev) {
        this.strDev = strDev;
    }

    public String getStrMod() {
        return strMod;
    }

    public void setStrMod(String strMod) {
        this.strMod = strMod;
    }

    public String getStrSo() {
        return strSo;
    }

    public void setStrSo(String strSo) {
        this.strSo = strSo;
    }

    public String getStrPass() {
        return strPass;
    }

    public void setStrPass(String strPass) {
        this.strPass = strPass;
    }
}
