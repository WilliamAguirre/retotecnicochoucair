package co.com.choucair.retotecnico.stepdefinitions;


import co.com.choucair.retotecnico.Model.UTestData;
import co.com.choucair.retotecnico.tasks.Login;
import co.com.choucair.retotecnico.tasks.OpenUp;
import co.com.choucair.retotecnico.tasks.Register;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;

public class RetoChoucairStepDefinitions {

    @Before
    public void setSetage(){
        OnStage.setTheStage(new OnlineCast());
    }


    @Given("^than William wants to create a new user in Utest$")
    public void thanWilliamWantsToCreateANewUserInUtest(){
        OnStage.theActorCalled("William").wasAbleTo(OpenUp.thePage(), Login.onThePage());
    }


    @When("^he searches for JOIN TODAY on the Utest platform$")
    public void heSearchesForJOINTODAYOnTheUtestPlatform(List<UTestData> uTestData) throws Exception {
        OnStage.theActorInTheSpotlight().attemptsTo(Register.the(uTestData.get(0).getStrFirstName(),
                uTestData.get(0).getStrLastName(),
                uTestData.get(0).getStrEmail(),
                uTestData.get(0).getStrLanguage(),
                uTestData.get(0).getStrCity(),
                uTestData.get(0).getStrZip(),
                uTestData.get(0).getStrCountry(),
                uTestData.get(0).getStrDev(),
                uTestData.get(0).getStrMod(),
                uTestData.get(0).getStrSo(),
                uTestData.get(0).getStrPass()));
        System.out.println("Tamaño del List"+uTestData.size());
        for (int i=0; i<uTestData.size();i++) {
            System.out.println(uTestData.get(i)+""+i);
        }
    }

    @Then("^he finds the the title$")
    public void heFindsTheTheTitle(List<UTestData> uTestData) throws Exception{

    }
}
